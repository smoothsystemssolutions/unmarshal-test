package com.smooth.systems.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import com.smooth.systems.test.internet.Response;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UnmarshalTest {

  @Test
  public void unmarshalTestTagResponse() throws JsonParseException, JsonMappingException, IOException {
    XmlMapper xmlMapper = new XmlMapper();
    File inputFile = new File("src/test/resources/response/test_tag_response_prestashop_1-7-3.xml");
    TestWrapper tag = xmlMapper.readValue(inputFile, TestWrapper.class);
    log.info("Test: {}", tag);
    assertNotNull(tag.getIdLang());
    assertEquals("http://prestashop.local/api/languages/1", tag.getIdLang().getHref());
    assertEquals(new Long(57), tag.getIdLang().getIdLang());
  }

  @Test
  public void unmarshalTagsResponse() throws JsonParseException, JsonMappingException, IOException {
    XmlMapper xmlMapper = new XmlMapper();
    File inputFile = new File("src/test/resources/response/test_tags_response_prestashop_1-7-3.xml");
    TagsWrapper tag = xmlMapper.readValue(inputFile, TagsWrapper.class);
    log.info("Test: {}", tag);
    assertNotNull(tag.getIdLangs());
    assertEquals(2,tag.getIdLangs().size());
    assertEquals(new Long(57),tag.getIdLangs().get(0).getIdLang());
    assertEquals("http://prestashop.local/api/languages/1",tag.getIdLangs().get(0).getHref());
    assertEquals(new Long(34),tag.getIdLangs().get(1).getIdLang());
    assertEquals("http://prestashop.local/api/languages/2",tag.getIdLangs().get(1).getHref());
  }

  @Test
  public void unmarshalResponse() throws JsonParseException, JsonMappingException, IOException {
    XmlMapper xmlMapper = new XmlMapper();
    File inputFile = new File("src/test/resources/response/response.xml");
    Response resp = xmlMapper.readValue(inputFile, Response.class);
    log.info("Response: {}", resp);
//    assertNotNull(tag.getIdLang());
//    assertEquals("http://prestashop.local/api/languages/1", tag.getIdLang().getHref());
//    assertEquals(new Long(57), tag.getIdLang().getIdLang());
//    assertEquals(new Long(57), tag.getIdLang());
  }
}