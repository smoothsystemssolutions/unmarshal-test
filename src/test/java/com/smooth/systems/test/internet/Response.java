package com.smooth.systems.test.internet;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author annik
 *
 */
@Data
@XmlRootElement(name = "response")
//@XmlType( propOrder = { "category", "action", "code", "message"})
public class Response
{

    private Integer          code;
    private String           message;

    //    private GreetingResData  resData;
    private ResData resData;

    //
    @XmlTransient
    private Logger           LOG = LoggerFactory.getLogger(getClass());

    @XmlElement
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @XmlElements(value={@XmlElement})
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /** Getter.
     * @return the resData
     */
    public ResData getResData() {
        return resData;
    }

    /** Setter.
     * @param resData the resData to set
     */
    @XmlElement
    public void setResData(ResData resData) {
        this.resData = resData;
    }
}