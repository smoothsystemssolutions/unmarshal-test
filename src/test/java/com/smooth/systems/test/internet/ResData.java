package com.smooth.systems.test.internet;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by David Monichi <david.monichi@smooth-systems.solutions> on 27.05.18.
 */
@Data
@XmlRootElement
public class ResData {

    private List<XmlData> data;
}