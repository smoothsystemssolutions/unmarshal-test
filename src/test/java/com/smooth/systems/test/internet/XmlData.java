package com.smooth.systems.test.internet;

import lombok.Data;

import javax.xml.bind.annotation.*;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "data")
public class XmlData {

    @XmlAttribute(name = "name")
    private String name;

    @XmlValue
    private String value;
}