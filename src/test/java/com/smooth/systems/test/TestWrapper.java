package com.smooth.systems.test;


import javax.xml.bind.annotation.*;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

import java.util.List;

@Data
@XmlRootElement(name = "prestashop")
@XmlAccessorType(XmlAccessType.FIELD)
class TestWrapper {

//  @JsonProperty("id_lang")
//  @XmlElement(name = "id_lang")
//  private Long idLang;

  @JsonProperty("id_lang")
  private TagValue idLang;
}
