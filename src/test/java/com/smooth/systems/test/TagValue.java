package com.smooth.systems.test;

import javax.xml.bind.annotation.*;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "id_lang")
public class TagValue {

//  @XmlAttribute(name = "href")
  @JacksonXmlProperty(localName = "href")
  private String href;

//  @XmlValue
  @JacksonXmlText
  private Long idLang;
}